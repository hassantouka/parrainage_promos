<?php
   session_start();
   require_once 'Passerelles/pdoConnexion.php'; 
   require_once 'Passerelles/pdoCompte.php';
   require_once 'Passerelles/pdoSponsors.php';
   require_once 'Passerelles/pdoContact.php';
   require_once 'Passerelles/pdoEvenement.php';
   require_once 'Passerelles/pdoSubvention.php';
   require_once 'Passerelles/pdoStatistiques.php';
   include 'Vues/V_entete.php';

   if (!isset($_REQUEST['uc'])) {
    $_REQUEST['uc'] = 'accueil';
    include 'Vues/V_nav.php';
}
$uc = $_REQUEST["uc"];

switch ($uc) {
   
        case "gestion_etudiant":
        
        include 'Controleurs/C_gestion_etudiant.php';
        break;

        case "gestion_parrain":
        
        include 'Controleurs/C_gestion_parrain.php';
        break;
        case "gestion_entreprise":
       
        include 'Controleurs/C_gestion_entreprise.php';
        
        case "gestion_recherche":
        
        include 'Controleurs/C_gestion_recherche.php';
        break;
    
        default:
        include 'Vues/V_accueil.php';
        break;
}
include 'Vues/V_pied.php';
