<?php

/**
 * Classe d'accès aux données PdoPracticiens. 
 * @author Hassan Touka
 * @version    1.0s
 */
class PdoVille {
    /* Fonction qui va obtenir tout les parrain */

    public static function getVille($nomVille, $nomDep) {
        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req = "select ville_id "
                    . "from villes_france_free "
                    . "where ville_nom_reel = '$nomVille' and ville_departement = $nomDep";
            $res = $objPdo->query($req);
            $lesLignes = $res->fetchAll();

            return $lesLignes;
        } catch (Exception $ex) {
            echo $ex;
            return false;
        }
    }
}
    