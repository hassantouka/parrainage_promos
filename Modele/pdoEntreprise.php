<?php

/**
 * Classe d'accès aux données Pdoentreprise. 
 * @author Hassan Touka
 * @version    1.0s
 * date 06/2018
 */
class PdoEntreprise {
    /* Fonction pour obtenir les entreprises */

    public static function getLesEntreprise() {
        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req = "select entreprise.numEntreprise, nomEntreprise,activiteentreprise.Libelle,
                    villes_france_free.ville_nom "
                    . "from entreprise inner join villes_france_free "
                    . "on villes_france_free.ville_id = entreprise.villeEntreprise "
                    . "inner join activiteentreprise on "
                    . "activiteentreprise.Code = entreprise.activiteEntreprise "
                    . "order by entreprise.nomEntreprise";

            print_r($req);

            $res = $objPdo->query($req);
            $lesEntreprise = $res->fetchAll();
            $res->closeCursor();

            return $lesEntreprise;
        } catch (Exception $ex) {
            echo $ex;
            return false;
        }
    }

    public static function selectUneEntreprise($numEntreprise) {
        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req = "select *
	from entreprise
	where numEntreprise = $numEntreprise ";

            $res = $objPdo->query($req);

            $lesLignes = $res->fetchAll();
        } catch (Exception $ex) {
            echo $ex;
            return false;
        }
    }

    /* Fonction qui va selectionner tout */

    public static function selectEntreprise($rechercheEntreprise, $rechercheN, $rechercheA) {
        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            if ($rechercheEntreprise == NULL && $rechercheN == NULL && $rechercheA == NULL) {
                $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                        . "from entreprise inner join villes_france_free on "
                        . "villes_france_free.ville_id = entreprise.villeEntreprise "
                        . "inner join activiteentreprise on "
                        . "activiteentreprise.Code = entreprise.activiteEntreprise ";
                //."where ville_nom like '$rechercheEntreprise%' AND entreprise.nomEntreprise like '$rechercheN%' AND activiteentreprise.Code like '$rechercheA%' ";
            } elseif ($rechercheEntreprise != NULL && $rechercheN != NULL && $rechercheA != NULL) {
                $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                . "from entreprise inner join villes_france_free on "
                . "villes_france_free.ville_id = entreprise.villeEntreprise "
                . "inner join activiteentreprise on "
                . "activiteentreprise.Code = entreprise.activiteEntreprise "
                ."where ville_nom like '$rechercheEntreprise%' AND entreprise.nomEntreprise like '$rechercheN%' AND activiteentreprise.Code like '$rechercheA%' ";
            } elseif ($rechercheEntreprise != NULL && $rechercheN == NULL && $rechercheA == NULL) {
                $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                        . "from entreprise inner join villes_france_free on "
                        . "villes_france_free.ville_id = entreprise.villeEntreprise "
                        . "inner join activiteentreprise on "
                        . "activiteentreprise.Code = entreprise.activiteEntreprise "
                        . "where ville_nom like '$rechercheEntreprise%'";
            } elseif ($rechercheEntreprise == NULL && $rechercheN != NULL && $rechercheA == NULL) {
                $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                        . "from entreprise inner join villes_france_free on "
                        . "villes_france_free.ville_id = entreprise.villeEntreprise "
                        . "inner join activiteentreprise on "
                        . "activiteentreprise.Code = entreprise.activiteEntreprise "
                        . "where entreprise.nomEntreprise like '$rechercheN%'";
            } elseif ($rechercheEntreprise == NULL && $rechercheN == NULL && $rechercheA != NULL) {

                $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                        . "from entreprise inner join villes_france_free on "
                        . "villes_france_free.ville_id = entreprise.villeEntreprise "
                        . "inner join activiteentreprise on "
                        . "activiteentreprise.Code = entreprise.activiteEntreprise "
                        . "where  activiteentreprise.Code like '$rechercheA%' ";
            } elseif ($rechercheEntreprise == NULL && $rechercheN != NULL && $rechercheA != NULL) {

                $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                        . "from entreprise inner join villes_france_free on "
                        . "villes_france_free.ville_id = entreprise.villeEntreprise "
                        . "inner join activiteentreprise on "
                        . "activiteentreprise.Code = entreprise.activiteEntreprise "
                        . "where  activiteentreprise.Code like '$rechercheA%' AND entreprise.nomEntreprise like '$rechercheN%' ";
            } elseif ($rechercheEntreprise != NULL && $rechercheN == NULL && $rechercheA != NULL) {
                $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                        . "from entreprise inner join villes_france_free on "
                        . "villes_france_free.ville_id = entreprise.villeEntreprise "
                        . "inner join activiteentreprise on "
                        . "activiteentreprise.Code = entreprise.activiteEntreprise "
                        . "where  activiteentreprise.Code like '$rechercheA%' AND ville_nom like '$rechercheEntreprise%'  ";
            } elseif ($rechercheEntreprise != NULL && $rechercheN != NULL && $rechercheA == NULL) {
                $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                        . "from entreprise inner join villes_france_free on "
                        . "villes_france_free.ville_id = entreprise.villeEntreprise "
                        . "inner join activiteentreprise on "
                        . "activiteentreprise.Code = entreprise.activiteEntreprise "
                        . "where  entreprise.nomEntreprise like '$rechercheN%' AND ville_nom like '$rechercheEntreprise%'  ";
            }

            $res = $objPdo->query($req);

            $lesLignes = $res->fetchAll();
            return $lesLignes;
        } catch (Exception $ex) {
            echo $ex;
            return false;
        }
    }

    public static function selectNomEntreprise($rechercheN) {
        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                    . "from entreprise inner join villes_france_free on "
                    . "villes_france_free.ville_id = entreprise.villeEntreprise "
                    . "inner join activiteentreprise on "
                    . "activiteentreprise.Code = entreprise.activiteEntreprise "
                    . "where entreprise.nomEntreprise like '$rechercheN%' ";

            $res = $objPdo->query($req);

            $lesLignes = $res->fetchAll();
            return $lesLignes;
        } catch (Exception $ex) {
            echo $ex;
            return false;
        }
    }

    public static function selectActiviteEntreprise($rechercheA) {
        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req = "select entreprise.numEntreprise,entreprise.nomEntreprise,activiteentreprise.Libelle,villes_france_free.ville_nom "
                    . "from entreprise inner join villes_france_free on "
                    . "villes_france_free.ville_id = entreprise.villeEntreprise "
                    . "inner join activiteentreprise on "
                    . "activiteentreprise.Code = entreprise.activiteEntreprise "
                    . "where activiteentreprise.Code like '$rechercheA%' ";

            $res = $objPdo->query($req);

            $lesLignes = $res->fetchAll();
            return $lesLignes;
        } catch (Exception $ex) {
            echo $ex;
            return false;
        }
    }

    public static function AjouterEntreprise($numEntreprise, $nomEntreprise, $activiteEntreprise, $villeEntreprise) {
        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $nomCom = addslashes($nomEntreprise);
            $activiteCom = addslashes($activiteEntreprise);
            $villeCom = addslashes($villeEntreprise);


            $req = utf8_decode("INSERT INTO entreprise "
                    . "(numEntreprise, nomEntreprise, activiteEntreprise, villeEntreprise)"
                    . " VALUES($numEntreprise, '$nomCom','$activiteCom', '$villeCom')");
            $objPdo->exec($req);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function countEntreprise() {
        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req = "SELECT COUNT (*) as nbEntreprise FROM entreprise ";

            $res = $objPdo->query($req);

            $lesLignes = $res->fetchAll();

            return $lesLignes;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function pageEntreprise($cPage, $parPage) {
        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req = "select entreprise.numEntreprise, nomEntreprise,activiteentreprise.Libelle,
                    villes_france_free.ville_nom "
                    . "from entreprise inner join villes_france_free "
                    . "on villes_france_free.ville_id = entreprise.villeEntreprise "
                    . "inner join activiteentreprise on "
                    . "activiteentreprise.Code = entreprise.activiteEntreprise "
                    . "order by entreprise.nomEntreprise "
                    . "LIMIT $cPage,$parPage";


            $res = $objPdo->query($req);

            $lesLignes = $res->fetchAll();

            return $lesLignes;
        } catch (Exception $ex) {
            return false;
        }
    }

}

?>
