<?php
/** 
 * Classe d'accès aux données PdoPracticiens. 
 * @author Hassan Touka
 * @version    1.0s
 */

class PdoParrain{   	
/*Fonction qui va obtenir tout les parrain*/
	public static function getParrain(){
              try {
		$objPdo = PdoConnexion::getPdoConnexion();
		$req = "select ancienetudiant.numAncienE,nom,prenom,mail,anneeBts "
                        . "from ancienetudiant "
                     ///   . "inner join formationeffectue on "
//                        . "formationeffectue.idEtudiant = ancienetudiant.numAncienE "
//                        . "inner join emploi on "
//                        . "emploi.numAncienE = ancienetudiant.numAncienE "
//                        . "inner join parrainage on "
//                        . "parrainage.numParrain = ancienetudiant.numAncienE "
                        . "order by ancienetudiant.numAncienE";
        	$res = $objPdo->query($req);
		$lesLignes = $res->fetchAll();
                	
                return $lesLignes;
          }
      catch (Exception $ex) {
            echo $ex;
            return false;} 
        }

/*Fonction pour selectionner un parrain*/
	public static function selectParrain($recherche){
              try {
	$objPdo = PdoConnexion::getPdoConnexion();
	$req = "select *
	from ancienetudiant
	where nom LIKE '$recherche%'"; 	
	
    	$res = $objPdo->query($req);
	$lesLignes = $res->fetchAll();
       
	return $lesLignes; 
     }
      catch (Exception $ex) {
            echo $ex;
            return false;} 
        }
        
         public static function AjouterParrain($numAncienE, $nom, $prenom, $mail, $anneeBts){
      try{
          $objPdo = PdoConnexion::getPdoConnexion();
          $nomCom = addslashes($nom);
          $prenomCom = addslashes($prenom);
          $mailCom = addslashes($mail);
          
          $req = utf8_decode("INSERT INTO ancienetudiant "
          ." VALUES($numAncienE, '$nomCom','$prenomCom', '$mailCom',$anneeBts)");
          $objPdo->exec($req);
          return true;
      } catch (Exception $ex) {
          return false;
      }      
	
      
      }
      
      	public static function getFilleul($numAncienE){
              try {
		$objPdo = PdoConnexion::getPdoConnexion();
		$req = "select ancienetudiant.numAncienE,ancienetudiant.nom,ancienetudiant.prenom,etudiant.numEtudiant,etudiant.nom AS nomEtudiant,etudiant.prenom AS prenomEtudiant "
                        . "from parrainage "
                        . "inner join ancienetudiant on ancienetudiant.numAncienE = parrainage.numParrain "
                        . "inner join etudiant on etudiant.numEtudiant = parrainage.numEtudiant "
                        . "where ancienetudiant.numAncienE = $numAncienE";                
        	$res = $objPdo->query($req);
		print_r($req);
                $lesLignes = $res->fetchAll();
                	
                return $lesLignes;
          }
      catch (Exception $ex) {
            echo $ex;
            return false;} 
        }
      
      
      
      
      
      
      
}

?>
