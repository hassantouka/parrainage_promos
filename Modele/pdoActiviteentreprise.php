<?php
class PdoActiviteentreprise{   	
 /*Fonction */
	public static function RecuperationActivite() {
        $objPdo = PdoConnexion::getPdoConnexion();
        $req = "SELECT Code,Libelle "
                . "FROM activiteentreprise "
                . "ORDER BY Code";
        $res = $objPdo->query($req);
        $LesActivite = $res->fetchAll();
        $res->closeCursor();
        return $LesActivite;
    }
    
    public static function RecuperationVilles() {
        $objPdo = PdoConnexion::getPdoConnexion();
        $req = "SELECT ville_id,ville_nom "
                . "FROM villes_france_free "
                . "ORDER BY ville_id";
        $res = $objPdo->query($req);
        $LesVilles = $res->fetchAll();
        $res->closeCursor();
        return $LesVilles;
    }
    
    
}