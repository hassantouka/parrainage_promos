$('#listeEntreprise').change(function () {
    var resultat = document.getElementById("ResultatContacts");
    $.ajax({
        url: 'Passerelles/getEntrepriseJSON.php',
        type: 'POST',
        data: 'code=' + $('#listeEntreprise').val(),
        datatype: 'json',
        success: function (data) {
            resultat.innerHTML = "";
            var tableau = "<table class='table table-striped'> <thead> <tr> <th>Nom de l'entreprise</th> <th>Activité de l'entreprise</th> <th>Ville</th>  </tr> </thead> <tbody>";
            resultat.innerHTML += "<h3>Liste des entrepirse  contacts du sponsor " + listeSponsors.options[listeSponsors.selectedIndex].text + "</h3>";
            for(var index in data)
            {
                tableau += "<tr> <td>" + data[index].nomContact + "</td> <td>" + data[index].prenomContact + "</td> <td>" + data[index].telephoneContact + "</td> <td>" + data[index].emailContact + "</td> </tr>";
            }
            tableau += "</tbody> </table>";
            resultat.innerHTML += tableau;
        },
        error: function () {
            alert("erreur !");
        }
    });
}
);