function infoParrain(numAncienE)
{
var resultat = document.getElementById("resultat");

$.ajax({
	url: 'Modele/getParrainJSON.php',
	type: 'POST',
	data: 'numAncienE=' + numAncienE,
	datatype: 'json',

	success : function (data) {
		resultat.innerHTML = "<h3><strong>Informations sur les Parrains : </strong></h3><br />";
		$.each(JSON.parse(data), function(key, value){
			resultat.innerHTML += "<p>Numéro du parrain : " + value.numAncienE + "</p>";
			resultat.innerHTML += "<p>Nom : " + value.nom + "</p>";
			resultat.innerHTML += "<p>Prénom : " + value.prenom + "</p>";
			resultat.innerHTML += "<p>Mail : " + value.mail + "</p>";
			resultat.innerHTML += "<p>Année BTS : " + value.anneeBts + "</p>";
		
		});
	},
	
	error: function () {
		alert("Une erreur a été détectée !");
	}
});
}