<div class="bloc">
    <div class="container">
        <h3>Ajouter un etudiant</h3>
        <span>* indique un champ obligatoire</span>
        <form method="post" action="index.php?uc=controleur&action=etudiant_ajout_valider">
            <div class="row">
                <fieldset class="form-group">
                    <legend class="col-md-8">Identification</legend>
                    <div class="input-group input-group-sm col-md-6">
                        <span class="input-group-addon"><i class="glyphicon" >id</i></span>
                        <input type="number" name="numEtudiant" class="form-control" placeholder="Numéro d'identification de l'étudiant *" required="required" />
                    </div>
                    <div class="input-group input-group-sm col-md-6">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="text" name="nom" class="form-control" placeholder="Nom de l'étudiant *" required="required"/> 
                    </div>
                </fieldset>
                <fieldset class="form-group"> 
                  
                    <div class="input-group input-group-sm col-md-6">
                        <span class="input-group-addon"><i class="glyphicon " ></i></span>
                        <input type="text" name="prenom" class="form-control" placeholder="prenom de l'étudiant*" required="required"/>
                    </div>
                    <div class="input-group input-group-sm col-md-6">
                        <span class="input-group-addon"><i class="glyphicon "></i></span>
                        <input type="text" name="classe" class="form-control" placeholder="classe de l'étudiant*" required="required"/>

                    </div>
                    <div class="input-group input-group-sm col-md-6">
                        <span class="input-group-addon"><i class="glyphicon "></i></span>
                        <fieldset>
                            <legend>Choisir une specialité</legend>

                            <div>
                                <input type="radio" id="radio" name="specialite"  />
                                <label for="SISR">SISR</label>
                            </div>

                            <div>
                                <input type="radio" id="radio" name="specialite" />
                                <label for="SLAM">SLAM</label>
                            </div>
                            
                        </fieldset>
                    </div>

                </fieldset>

                <fieldset class="form-group col-md-8"> <legend class="col-md-8">  </legend>
                    <div class="input-group input-group-sm col-md-6">
                        <br/>
                        <input class="btn btn-default" type="submit" value="Valider" /> <input class="btn btn-default" type="reset" value="Annuler" />
                    </div>
                </fieldset>
            </div>
        </form>
    </div>  
</div>
