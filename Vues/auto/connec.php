<?php

$dbhost = 'localhost';
$dbname = 'parrainage_promos';
$dbuser = 'root';
$dbpass = '';

try {
    $dbcon = new PDO ("mysql:host={$dbhost};dbname={$dbname}",$dbuser,$dbpass);
    $dbcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException$ex){
    
    die($ex->getMessage());
}
$stmt=$dbcon->prepare('SELECT * FROM entreprise');
$stmt->execute();
$array = array();
while($row=$stmt-> fetch(PDO::FETCH_ASSOC))
{
    extract($row);
    $array[]= $nomEntreprise;
}
        echo json_encode($array);
        
    ?>