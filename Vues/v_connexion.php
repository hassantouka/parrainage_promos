<div class="bloc">
<div class="container">
    <h3>Connexion</h3>
    <form method="post" action="index.php?uc=connexion_compte&action=tentative_connexion">
        <div class="row">
            <fieldset class="form-group">
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user" >id</i></span>
                    <input type="text" name="mail" class="form-control" placeholder="Adresse mail" required="required" />
                </div>
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                    <input type="password" name="mdp" class="form-control" placeholder="Mot de passe" required="required"/>    
                </div>
            </fieldset>
            <fieldset class="form-group col-md-8"> <legend class="col-md-8">  </legend>
                <div class="input-group input-group-sm col-md-6">
                    <br/>
                    <input class="btn btn-default" type="submit" value="Valider" /> <input class="btn btn-default" type="reset" value="Annuler" />
                </div>
            </fieldset>
        </div>
    </form>
</div>
</div>