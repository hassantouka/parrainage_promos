<div class="bloc">
<div class="container">
    <h3>Ajouter une entreprise</h3>
    <span>* indique un champ obligatoire</span>
    <form method="post" action="index.php?uc=gestion_entreprise&action=entreprise_ajout_valider">
        <div class="row">
            <fieldset class="form-group">
                <legend class="col-md-8">Identification</legend>
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon" >id</i></span>
                    <input type="number" name="numEntreprise" class="form-control" placeholder="Numéro d'identification *" required="required" />
                </div>
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" name="nomEntreprise" class="form-control" placeholder="Nom de l'entreprise *" required="required"/> 
                </div>
            </fieldset>
            <fieldset class="form-group"> 
                
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-map-marker"></i></span>
                     <input type="text" name="rechercheEntreprise"  id="recherche" class="form-control" ui-autocomplete-loading placeholder ="ville" required="required"/>
                       
                </div>
             
            </fieldset>
               <fieldset class="form-group"> <legend class="col-md-8">Activite</legend>
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-book"></i></span>
                    <select name="activiteEntreprise" class="form-control">
                        <?php
                        foreach ($LesActivite as $uneActivite) {
                            echo '<option value="' .  $uneActivite['Code'] . '">' .  $uneActivite['Libelle'] . '</option>';
                        }
                        ?>
                    </select>   
                </div>
            </fieldset>
            <fieldset class="form-group col-md-8"> <legend class="col-md-8">  </legend>
                <div class="input-group input-group-sm col-md-6">
                    <br/>
                    <input class="btn btn-default" type="submit" value="Valider" /> <input class="btn btn-default" type="reset" value="Annuler" />
                </div>
            </fieldset>
        </div>
    </form>
</div>  
</div>

   
<script>
   
$('#recherche').autocomplete({

    source : 'Modele/fetch.php'

});
</script>