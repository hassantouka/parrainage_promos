<div class="container">
    <legend>Liste des Activités</legend>
    <div class="row">
        <table class="table table-striped"> 
            <thead> 
                <tr>
                    <td>Code</td>
                    <td>Libelle</td> 

                </tr> 
            </thead> 
            <tbody> 
                <?php
                foreach ($LesActivite as $activite) {
                    ?>
                    <tr> 
                        <td><?php echo $activite['Code'] ?></td>
                        <td><?php echo $activite['Libelle'] ?></td>
             
                    <?php
                }
                ?>
            </tbody> 
        </table>
    </div>
</div>

