<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php?uc=accueil"> <img src="img/logo.png" id="logo"> </a>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#barre-de-navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

            </button>
        </div>

        <div class="collapse navbar-collapse" id="barre-de-navigation">
            <?php if (isset($_SESSION['id'])) { ?>
                <ul class="nav navbar-nav">

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Etudiant<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <div id="menuderoul">
                                <li><a href="index.php?uc=gestion_etudiant&action=etudiant">Etudiant 1ère année</a></li>
                                <li><a href="index.php?uc=gestion_etudiant&action=etudiant">Etudiant 2ème année</a></li>
                                <li><a href="index.php?uc=gestion_etudiant&action=etudiant_ajout">Etudiant ajout</a></li>


                            </div>
                        </ul>
                    </li>


                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Parrain<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <div id="menuderoul">
                                <li><a href="index.php?uc=gestion_parrain&action=parrain">Parrain liste</a></li>
                                <li><a href="index.php?uc=gestion_parrain&action=parrain_ajout">Parrain ajout </a></li>
                                <li><a href="index.php?uc=gestion_parrain&action=parrain_filleul">Parrain filleul</a></li>
                                <li><a href="index.php?uc=gestion_parrain&action=parrain_info">Parrain information</a></li>
                            </div>
                        </ul>
                    </li>

                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Entreprise<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <div id="menuderoul">
                                <li><a href="index.php?uc=gestion_entreprise&action=entreprise">Entreprise liste</a></li>
                                <li><a href="index.php?uc=gestion_entreprise&action=entreprise_ajout">Entreprise ajout</a></li>


                            </div>
                        </ul>
                    </li>


                    <li class ="nav-item">
                        <a class="nav-link" href="index.php?uc=gestion_recherche&action=gestion">Gestion</a></li>


                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"><?php echo $_SESSION['id'] ?></span></a></li>
                    <li><a href="index.php?uc=connexion_compte&action=deconnexion"><span class="glyphicon glyphicon-log-out"></span>Déconnexion</a></li>
                </ul> 
            <?php }
            ?>

            <?php if (!isset($_SESSION['id'])) { ?>

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="index.php?uc=connexion_compte&action=connexion"><span class="glyphicon glyphicon-log-in"></span>Connexion</a></li>
                </ul>
            </div>    
            <?php
            }
            ?>

    </div>    
</nav>


