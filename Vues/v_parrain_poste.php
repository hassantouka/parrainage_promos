<div class="bloc">
<div class="container">    
	<h2>Liste des  postes du Parrain <?php  echo $nom ?></h2>
        
        <div class="row">
            <table class="table table-striped"> 
                <thead> 
                    <tr> 
             
                        <td>Num du poste</td> 
                        <td>Nom du poste</td>
                        <td>Entreprise</td>
                        <td>Année d'entrée</td>
                        <td>Année de sortie</td>
                        <td>Ville</td>
                        
                    </tr> 
                </thead> 
                <tbody> 

                    <?php
                    
                        foreach ($lesPostes as $unPoste) {
                            
                        
                        ?>
                    
                        <tr> 
                           
                            <td><?php echo $unPoste['idPoste'] ?></td>
                             <td><?php echo $unPoste['nomPoste'] ?></td>
                              <td><?php echo $unPoste['nomEntreprise'] ?></td>
                               <td><?php echo $unPoste['anneeEmbauche'] ?></td>
                                <td><?php echo $unPoste['anneeSortie'] ?></td>
                                 <td><?php echo $unPoste['ville_nom_reel'] ?></td>
                        </tr>
                        <?php
                    }
                    
                    ?>
                </tbody> 

            </table>

        </div>
</div>
</div>
<script src="lib/infoEtudiant.js" type="text/javascript"></script>