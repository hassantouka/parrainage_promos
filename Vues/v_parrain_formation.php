<div class="bloc">
<div class="container">    
	<h2>Liste des  formation du Parrain <?php  echo $nom ?></h2>
        
        <div class="row">
            <table class="table table-striped"> 
                <thead> 
                    <tr> 
             
                        <td>Num de la formation </td> 
                        <td>Libelle de la formation</td>
                        <td>Ville de la formation</td>
             
                    </tr> 
                </thead> 
                <tbody> 

                    <?php
                    
                        foreach ($lesFormations as $uneFormation) {
                            
                        
                        ?>
                    
                        <tr> 
                           
                            <td><?php echo $uneFormation['numFormation'] ?></td>
                             <td><?php echo $uneFormation['libelleFormation'] ?></td>
                              <td><?php echo $uneFormation['ville_nom_reel'] ?></td>
                        </tr>
                        <?php
                    }
                    
                    ?>
                </tbody> 

            </table>

        </div>
</div>
</div>
<script src="lib/infoEtudiant.js" type="text/javascript"></script>