    <div class="container">
        <div class="panel panel-primary col-md-12">
        <h2>Liste des entreprises</h2>

        <div class="row">
            <table class="table table-striped"> 
                <thead> 
                    <tr> 
                        <td>Num de l'entreprise </td>
                        <td>Nom de l'entreprise</td> 
                        <td>Activité</td> 
                        <td>Ville</td> 

                    </tr> 
                </thead> 
                <tbody> 

                    <?php
                   
                    foreach($lesEntreprise as $uneEntreprise) {
                        ?>
                    
                        <tr> 
                            <td><?php echo $uneEntreprise['numEntreprise'] ?> </td>
                            <td><?php echo $uneEntreprise['nomEntreprise'] ?></td>
                            <td><?php echo $uneEntreprise['Libelle'] ?></td>
                            <td><?php echo $uneEntreprise['ville_nom'] ?></td>

                        </tr>
                        <?php
                    }
                    
                    ?>
                </tbody> 

            </table>

        </div>


        <ul class="pager">

            <li><a href="index.php?uc=gestion_entreprise&action=entreprise&add=<?php echo $cpage - 5; ?>">Précédent</a></li>

            <li><a href="index.php?uc=gestion_entreprise&action=entreprise&add=<?php echo $cpage +5  ?>">Suivant</a></li>

        </ul>
    </div>
</div>