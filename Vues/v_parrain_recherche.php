<div class="bloc">
<div class="container">    

        <form class="navbar-form navbar-right inline-form" method="POST" action="index.php?uc=gestion_parrain&action=parrain_recherche">
      <div class="form-group">
        <input type="search" name="rechercheParrain" class="input-sm form-control" placeholder="Recherche">
        <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
      </div>
    </form>
    
    
	<h2>Liste des Parrains</h2>

        <div class="row">
            <table class="table table-striped"> 
                <thead> 
                    <tr> 
                        <td>Num du Parrain</td>
                        <td>Nom du Parrain</td> 
                        <td>Prenom du Parrain</td> 
                        <td>Mail</td> 
                        <td>Année BTS</td> 
                    </tr> 
                </thead> 
                <tbody> 

                    <?php
                    foreach ( $LesParrain as $unParrain) {
                        ?>
                    
                        <tr> 
                            <td><?php echo $unParrain['numAncienE'] ?> </td>
                            <td><?php echo $unParrain['nom'] ?></td>
                            <td><?php echo $unParrain['prenom'] ?></td>
                            <td><?php echo $unParrain['mail'] ?></td>
                             <td><?php echo $unParrain['anneeBts'] ?></td>

                        </tr>
                        <?php
                    }
                    
                    ?>
                </tbody> 

            </table>

        </div>
</div>
</div>
<script src="lib/infoEtudiant.js" type="text/javascript"></script>