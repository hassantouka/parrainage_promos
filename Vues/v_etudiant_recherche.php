<div class="bloc">
    <div class="container">
        <h2>Liste des etudiant</h2>

        <form class="navbar-form navbar-right inline-form" method="POST" action="index.php?uc=gestion_etudiant&action=etudiant_recherche">
            <div class="form-group">
                <input type="search" class="input-sm form-control" placeholder="Recherche">
                <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
            </div>
        </form>     
        <div class="row">
            <table class="table table-striped"> 
                <thead> 
                    <tr> 
                        <td>Num de l'etudiant</td>
                        <td>Nom de l'etudiant</td> 
                        <td>prenom</td> 
                        <td>classe</td> 
                        <td>specialite</td> 
                    </tr> 
                </thead> 
                <tbody> 

                    <?php
                    foreach ($LesEtudiants as $unEtudiant) {
                        ?>

                        <tr> 
                            <td><?php echo $unEtudiant['numEtudiant'] ?> </td>
                            <td><?php echo $unEtudiant['nom'] ?></td>
                            <td><?php echo $unEtudiant['prenom'] ?></td>
                            <td><?php echo $unEtudiant['classe'] ?></td>
                            <td><?php echo $unEtudiant['specialite'] ?></td>

                        </tr>
                        <?php
                    }
                    ?>
                </tbody> 

            </table>

        </div>

        <div class="col-md-12">
            <div id="resultat"></div>
        </div>
    </div>
</div>
<script src="lib/infoEtudiant.js" type="text/javascript"></script>