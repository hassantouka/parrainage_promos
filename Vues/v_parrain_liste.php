<div class="bloc">
    <div class="container">
        <h2>Veuillez choisir le nom d'un parrain:</h2>
        <hr />
        
        <form class="navbar-form navbar-right inline-form" method="POST" action="index.php?uc=gestion_parrain&action=parrain_recherche">
            <div class="form-group">
                <input type="search" class="input-sm form-control" name="rechercheParrain" placeholder="Recherche">
                <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
            </div>
        </form>
       
    </div>
</div>
<script src="lib/infoParrain.js" type="text/javascript"></script>