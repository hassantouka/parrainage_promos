<div class="bloc">
    <div class="container">
        <h2>Veuillez choisir le nom d'un etudiant:</h2>
        <hr />
        <form class="navbar-form navbar-right inline-form" method="POST" action="index.php?uc=gestion_etudiant&action=etudiant_recherche">
            <div class="form-group">
                <input type="search" class="input-sm form-control" name="rechercheEtudiant" placeholder="Recherche">
                <button type="submit" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-eye-open"></span> Chercher</button>
            </div>
        </form>

        <form method="POST" action="index.php?uc=gestion_etudiant&action=etudiant">
            <div class="col-md-3">
                <label for="numEtudiant">Sélectionner un Etudiant :</label>
                <select class="form-control" name="numEtudiant" onchange="infoEtudiant(this.value)">

                    <option value="" disabled selected>Sélectionner</option>
                    <?php
                    foreach ($lesEtudiant as $unEtu) {
                        ?>
                        <option value="<?php echo $unEtu['numEtudiant']; ?>"> <?php echo $unEtu['nom']; ?> </option>

                    <?php }
                    ?>

                </select>
            </div>
        </form>

        <div class="col-md-12">
            <div id="resultat"></div>
        </div>
    </div>
</div>
<script src="lib/infoEtudiant.js" type="text/javascript"></script>