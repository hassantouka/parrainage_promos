<div class="bloc">
<div class="container">
    <h3>Ajouter un parrain</h3>
     </br>
     
    <span>* indique un champ obligatoire</span>
    <form method="post" action="index.php?uc=gestion_parrain&action=parrain_ajout_valider">
        <div class="row">
            <fieldset class="form-group">
                <legend class="col-md-8">Identification</legend>
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon" >id</i></span>
                    <input type="number" name="numAncienE" class="form-control" placeholder="Numéro d'identification du Parrain *" required="required" />
                </div>
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                    <input type="text" name="nom" class="form-control" placeholder="Nom du parrain *" required="required"/> 
                </div>
            </fieldset>
            <fieldset class="form-group"> 
             
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon " ></i></span>
                    <input type="text" name="prenom" class="form-control" placeholder="prenom du parrain*" required="required"/>
                </div>
                <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon "></i></span>
                    <input type="text" name="mail" class="form-control" placeholder="mail*" required="required"/>
                    
                </div>
                 <div class="input-group input-group-sm col-md-6">
                    <span class="input-group-addon"><i class="glyphicon "></i></span>
                    <input type="number" name="anneeBts" class="form-control" placeholder="Annee d'obtention du BTS*" required="required"/>
                   
                </div>
             
            </fieldset>
           
            <fieldset class="form-group col-md-8"> <legend class="col-md-8">  </legend>
                <div class="input-group input-group-sm col-md-6">
                    <br/>
                    <input class="btn btn-default" type="submit" value="Valider" /> <input class="btn btn-default" type="reset" value="Annuler" />
                </div>
            </fieldset>
        </div>
    </form>
</div>  
</div>
