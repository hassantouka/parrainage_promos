<?php
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'accueil';
}
$action = $_REQUEST["action"];

switch ($action) {
    //Si l'utilisateur essai de se connecter
    case "tentative_connexion":
        $mail = $_REQUEST['mail'];
        $mdp = $_REQUEST['mdp'];
        $res = PdoCompte::VerificationCompte($mail, $mdp);
        if ($res) {
            $_SESSION['id'] = $mail;
            $messageSucces = "Vous êtes bien connecté".$_SESSION['id'];
         
            include 'Vues/V_resultat.php';
        } else {
         
            $messageErreur = "Identifiants éronnés";
            include 'Vues/V_resultat.php';
            include 'Vues/v_connexion.php';
        }
        break;
        
    case "connexion":
   
        include 'Vues/v_connexion.php';
        break;
    
    case "deconnexion":
        unset($_SESSION['id']);
        session_destroy();
       
    
    default:
        include 'Vues/v_accueil.php';
        break;
    
}