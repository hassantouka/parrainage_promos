<?php

if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'action';
}
$action = $_REQUEST["action"];

switch ($action) {
    case "entreprise":

        $entreprise = PdoEntreprise::countEntreprise();

        $nbEntreprise = $entreprise['nbEntreprise'];
        $parPage = 5;
        
    if (!isset($_REQUEST['add'])) {
        $cpage = 0;
    } 
    else {
         $cpage = $_REQUEST['add'];
        if ($cpage >= 15) {
            $cpage = 0;
        }
        if ($cpage < 1) {
            $cpage = 0;
        }
    }
        $lesEntreprise = PdoEntreprise::pageEntreprise($cpage, $parPage);

     
        include 'Vues/v_entreprise_liste.php';
        break;


    case"entreprise_ajout":

        $lesEntreprise = PdoEntreprise::getLesEntreprise();
        $LesActivite = PdoActiviteentreprise::RecuperationActivite();
        $LesVilles = PdoActiviteentreprise::RecuperationVilles();
        include 'Vues/v_entreprise_ajout.php';

        break;

    case "entreprise_ajout_valider":

        $numEntreprise = $_REQUEST['numEntreprise'];
        $nomEntreprise = $_REQUEST['nomEntreprise'];
        $activiteEntreprise = $_REQUEST['activiteEntreprise'];
        $villeEntreprise = explode("*",$_REQUEST['rechercheEntreprise']);
        print_r($villeEntreprise);
        $villeId = PdoVille::getVille($villeEntreprise[0],$villeEntreprise[1]);
        $res = PdoEntreprise::AjouterEntreprise($numEntreprise, $nomEntreprise, $activiteEntreprise, $villeId[0][0]);
        if ($res) {
            $messageSucces = "$nomEntreprise a bien été ajouté.";
        } else {
            $messageErreur = "$nomEntreprise n'a pas pu être ajouté.";
        }
  
        include 'Vues/V_resultat.php';
        break;
  
    default:
        include 'Vues/v_accueil.php';
        break;
}
?>
