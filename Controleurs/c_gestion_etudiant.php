<?php
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'accueil';
}
$action = $_REQUEST["action"];
switch ($action) {
    case "etudiant":
        $lesEtudiant = PdoEtudiant::getEtudiant();
     
        include 'Vues/v_etudiant_liste.php';
        break;
    case "etudiant_ajout":
        $lesEtudiant = PdoEtudiant::getEtudiant();
      
        include 'Vues/v_etudiant_ajout.php';
        break;
    case "etudiant_ajout_valider":
        $numEtudiant = $_REQUEST['numEtudiant'];
        $nom = $_REQUEST['nom'];
        $prenom = $_REQUEST['prenom'];
        $classe = $_REQUEST['classe'];
        $specialite = $_REQUEST['specialite'];

        $res = PdoEtudiant::AjouterEtudiant($numEtudiant, $nom, $prenom, $classe, $specialite);
        if ($res) {
            $messageSucces = "$nom a bien été ajouté.";
        } else {
            $messageErreur = "$nom n'a pas pu être ajouté.";
        }
  
        include 'Vues/V_resultat.php';
        break;
        
    case "etudiant_recherche":
         $numEtudiant = $_REQUEST['rechercheEtudiant'];
     
        $LesEtudiants = PdoEtudiant::selectEtudiant($numEtudiant);
              
    
        include 'Vues/v_etudiant_recherche.php';
      
        break;
    
    default:
        include 'Vues/v_accueil.php';
        break;
}
include 'Vues/V_pied.php';