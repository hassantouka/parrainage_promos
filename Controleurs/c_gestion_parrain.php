<?php

if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'action';
}
$action = $_REQUEST["action"];

switch ($action) {
    case "parrain":
        $lesParrain = PdoParrain::getParrain();
       
        include 'Vues/v_parrain_liste.php';
        break;

    case "parrain_ajout":
        $LesParrain = PdoParrain::getParrain();
      
        include 'Vues/v_parrain_ajout.php';
        break;
  
    case "parrain_ajout_valider":
        $numAncienE = $_REQUEST['numAncienE'];
        $nom = $_REQUEST['nom'];
        $prenom = $_REQUEST['prenom'];
        $mail = $_REQUEST['mail'];
        $anneeBts = $_REQUEST['anneeBts'];

        $res = PdoParrain::AjouterParrain($numAncienE, $nom, $prenom, $mail, $anneeBts);
        if ($res) {
            $messageSucces = "$nom a bien été ajouté.";
        } else {
            $messageErreur = "$nom n'a pas pu être ajouté.";
        }
      
        include 'Vues/V_resultat.php';
        break;
        
    case "parrain_recherche":
        //rechercheParrain qui vient de la vue parrain liste
         if (!isset($_REQUEST['rechercheParrain'])) {
             $numAncienE = "";
         }
         else{
        $numAncienE = $_REQUEST['rechercheParrain'];
         }
        $LesParrain = PdoParrain::selectParrain($numAncienE);        
         
    
        include 'Vues/v_parrain_recherche.php';     
        break;
    
    case "parrain_filleul":
        
        $lesParrain = PdoParrain::getParrain();
        include'Vues/v_parrain_filleul.php';
        break;
    
     case "parrain_filleul_details":
        $det = $_REQUEST['det'];
         $nom = $_REQUEST['nom'];
         $lesFilleul = PdoParrain::getFilleul($det);
         
        include'Vues/v_parrain_filleul_details.php';
        break;
    
    case "parrain_info":
        
        $LesParrain = PdoParrain::getParrain();
        include'Vues/v_parrain_info.php';
               break;
           
           
    case "parrain_poste":
        
        $det = $_REQUEST['det'];
         $nom = $_REQUEST['nom'];
        
        $lesPostes = PdoInfo::getPoste($det);
        include 'Vues/v_parrain_poste.php';  
        break;
    
    
    case "parrain_formation":
        
        $det = $_REQUEST['det'];
         $nom = $_REQUEST['nom'];
       
        $lesFormations = PdoInfo::getFormation($det);
        
        include 'Vues/v_parrain_formation.php';
        break;

         
    default:
        include 'Vues/v_accueil.php';
        break;
}
?>

