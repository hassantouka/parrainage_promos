<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Parrainage promo</title>
        <meta charset="UTF-8">

        <link rel="stylesheet" type="text/css" href="BOOTSTRAP/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="style.css">
<!--        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>  -->

        <script src="BOOTSTRAP/jquery/jquery-3.3.1.js"></script>
        <script src="BOOTSTRAP/js/bootstrap.js"></script>
        <script type="text/javascript" src="jqueryui/js/jquery-ui-1.9.2.custom.min.js"></script>
        <link rel="stylesheet" type="text/css" href="jqueryui/css/ui-lightness/jquery-ui-1.9.2.custom.min.css">

    </head>
    <body>
        
        <?php
       session_start();
        require_once 'Modele/pdoConnexion.php';
        require_once 'Modele/pdoEntreprise.php';
        require 'Modele/pdoParrain.php';
        require 'Modele/pdoEtudiant.php';
        require 'Modele/pdoCompte.php';
        require 'Modele/pdoActiviteentreprise.php';
        require 'Modele/pdoVille.php';
        require 'Modele/pdoInfo.php';
        include 'Vues/v_navbar.php';
        if (!isset($_REQUEST['uc'])) {
        $_REQUEST['uc'] = 'uc';
        }
        $uc = $_REQUEST["uc"];


        switch ($uc) {

       case "connexion_compte":
        include 'Controleurs/c_connexion.php';
        break;

        
        case "gestion_etudiant":
              if(isset($_SESSION['id']))
      {
        include 'Controleurs/c_gestion_etudiant.php';
        break;
        }

       else{
        $messageErreur = "Veuillez vous connecter";
     
        include 'Vues/V_resultat.php';
        break;
        }

        case "gestion_entreprise":
            
       if(isset($_SESSION['id']))
        {

        include 'Controleurs/c_gestion_entreprise.php';
        

        break;
        }

        else{
        $messageErreur = "Veuillez vous connecter";
      
        include 'Vues/V_resultat.php';
        break;
        }


      

        case "gestion_parrain":
             if(isset($_SESSION['id']))
        {
        include 'Controleurs/c_gestion_parrain.php';
        break;
       }

        else{
        $messageErreur = "Veuillez vous connecter";
     
        include 'Vues/V_resultat.php';
        break;
        }


      
        case "gestion_recherche":
              if(isset($_SESSION['id']))
        {
        include 'Controleurs/c_gestion_recherche.php';
  
        break;
        }

        else{
        $messageErreur = "Veuillez vous connecter";
  
        include 'Vues/V_resultat.php';
        break;
        }



        case "accueil":
  
        include 'Vues/v_accueil.php';
        break;




        default:
    
        include 'Vues/v_accueil.php';

        break;
        }
        ?>
    </body>
</html>
